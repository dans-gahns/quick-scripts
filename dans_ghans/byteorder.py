""" Print out values of different by orderings of an input number. """

from __future__ import print_function, division
import sys


def dans_ghans_byte_representation(value):
    """ Byte encoding of integers for Py2 or 3 """
    if sys.version_info[0] < 3:  # If not python3
        return bytes(value)

    return bytes([value])


def dans_ghans_int_representation(value):
    """ Integer from byte encoding on Py2 or 3 """
    if sys.version_info[0] < 3:  # If not python3
        return ord(value)

    return value


def dans_ghans_get_packing(num_bytes, signed):
    """ Return the struct packing format character
        based on the number of bytes required."""
    return {0: ('B', 'b'),
            1: ('B', 'b'),
            2: ('H', 'h'),
            3: ('I', 'i'),
            4: ('I', 'i'),
            5: ('Q', 'q'),
            6: ('Q', 'q'),
            7: ('Q', 'q'),
            8: ('Q', 'q')}[num_bytes][signed]


def dans_ghans_encoding(packing, number):
    """ Compute various representations for given packed number. """
    import struct

    byte_list = [dans_ghans_int_representation(b)
                 for b in struct.pack(packing, number)]
    return {"bytes": byte_list,
            "hex": [hex(b) for b in byte_list]}


def dans_ghans_byte_order(number, signed_p=False, width=2):
    """ compute a dictionary of various byte orderings and permutations. """
    import struct
    import itertools

    packing = dans_ghans_get_packing(width, signed_p)

    native = dans_ghans_encoding('@' + packing, number)
    lee = dans_ghans_encoding('<' + packing, number)
    bee = dans_ghans_encoding('>' + packing, number)

    number_as_bytes = (struct.pack('B', dans_ghans_int_representation(b))
                       for b in struct.pack('@' + packing, number))

    other_vals = set((struct.unpack('@' + packing, b''.join(val))[0]
                      for val in itertools.permutations(number_as_bytes))) \
        .difference(set([number]))

    other_reprs = ((val, [dans_ghans_int_representation(b) for b in rep],
                    [hex(dans_ghans_int_representation(b)) for b in rep])
                   for rep, val in ((struct.pack('@' + packing, v), v)
                                    for v in other_vals))

    return \
        {"bit pattern": format(number, 'b'),
         "native": native,
         "little endian": lee,
         "big endian": bee,
         "other permutations": [{"value": vl,
                                 "bytes": bt,
                                 "hex": hx} for (vl, bt, hx) in other_reprs]}


if __name__ == "__main__":
    import pprint
    import math

    NUMBER = int(sys.argv[1])
    WIDTH = int(math.ceil(NUMBER.bit_length() / 8)) + int(NUMBER < 0)
    RESULT = dans_ghans_byte_order(NUMBER, NUMBER < 0, WIDTH)
    NUM_OTHERS = len(RESULT["other permutations"])
    if NUM_OTHERS > 50:
        RESULT["other permutations"] = str(NUM_OTHERS) \
                                       + " alternatives not printed."

    pprint.pprint(RESULT)
