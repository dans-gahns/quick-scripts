---
title: Quick scripts introduction
description: 'Some quick and useful scripts to quickly accomplish small tasks.'
---


# Quick Scripts
---

*Some quick and useful scripts to quickly accomplish small tasks.*

This repository contains the following scripts:

| Script name    | Description                     | Example            |
|:---------------|:--------------------------------|:-------------------|
| byteorder.py   | Display byte order permutations |`byteorder.py 1024` |

## Running a script
These scripts do not depend (at run-time) on external python modules. Therefore yout can simply
run them with:
```bash
python script [arguments]
```

## Development on Windows
The following instructions are a one-time-setup. After you finish you do not have to repeat them.

Make sure pip is installed and up to date with:
```bash
py -m pip version
py -m pip install --upgrade pip
```

Install virtualenv with:
```bash
py -m pip install --user virtualenv
```

Create and activate a virtual environment:
```bash
py -p python3 -m venv env
.\env\Scripts\activate
```

Install required packages into the new virtual environment:
```bash
py -m pip install -r requirements.txt
```

Finally, install the dans-ghans package:
```bash
py -m pip install -e .
```
This completes the one-time-setup of the development environment. After you finish, deactivate the virtual environment with:
```bash
deactivate
```

When you come back and want to keep working on the project, simply run:
```bash
.\env\Scripts\activate
```
