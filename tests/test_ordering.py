from dans_ghans import byteorder
import pytest
from hypothesis import given, settings
import hypothesis.strategies as st
import math

def test_simple():
    ordering = byteorder.dans_ghans_byte_order(0)
    assert ordering["bit pattern"] == '0'


@settings(deadline=500.0)
@given(st.integers(min_value=-((2**56) + 1), max_value=((2**64) - 1)))
def test_no_crash(i):
    WIDTH = int(math.ceil(i.bit_length() / 8)) + int(i < 0)
    assert "native" in byteorder.dans_ghans_byte_order(i, i < 0, WIDTH)
